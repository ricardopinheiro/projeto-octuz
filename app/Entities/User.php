<?php

namespace App\Entities;

use App\Entities\Issue;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'name',
        'email',
        'password',
        'address',
        'cpf',
        'dt_nascimento',
        'hash',
        'path_url',
        'facebook_id',
        'fone',
        'role'
    ];

    public function account()
    {
        return $this->hasMany(\App\Entities\UserAccount::class, 'client_id');
    }


}

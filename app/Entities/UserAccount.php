<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserAccount extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    protected $fillable = [
        'numero_conta',
        'client_id',
        'login',
        'senha',
        'employer_id'
    ];

    public function empresa()
    {
        return $this->belongsTo(\App\Entities\Employer::class, 'employer_id');
    }

}

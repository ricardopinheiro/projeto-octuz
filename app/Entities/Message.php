<?php

namespace App\Entities;

use Firebase\FirebaseLib;
use Illuminate\Database\Eloquent\Model;
use Mpociot\Firebase\SyncsWithFirebase;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Message extends Model implements Transformable
{
    use TransformableTrait, SyncsWithFirebase;

    //protected $appends = ['user'];

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = [
        'message',
        'status',
        'user_id',
        'issue_id',
    ];

    protected function saveToFirebase($mode)
    {
        if (is_null($this->firebaseClient)) {
            $this->firebaseClient = new FirebaseLib(config('services.firebase.database_url'), config('services.firebase.secret'));
        }
        $path = $this->getTable() . '/' . str_pad($this->fresh()->issue_id, 5, "0", STR_PAD_LEFT) . '/' . $this->getKey() . '/';

        if ($mode === 'set') {
            $this->firebaseClient->set($path, $this->getFirebaseSyncData());
        } elseif ($mode === 'update') {
            $this->firebaseClient->update($path, $this->getFirebaseSyncData());
        } elseif ($mode === 'delete') {
            $this->firebaseClient->delete($path);
        }
    }

    protected function getFirebaseSyncData()
    {

        if ($fresh = $this->fresh()) {

            $arr = $fresh->toArray();
            $arr['id'] =  str_pad($arr['id'], 5, "0", STR_PAD_LEFT);
            $arr['user']['id'] = $this->user->id;
            $arr['user']['name'] = $this->user->name;
            $arr['user']['path_url'] = $this->user->path_url;

            return $arr;
        }

        return [];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}

<?php

namespace App\Entities;

use App\Entities\IssusNote;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Issue extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'description',
        'solution_description',
        'issue_status',
        'hint_problema',
        'hint_solucao',
        'category_id',
        'client_id',
        'employer_id',
        'account_id'
    ];

    public function cliente()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function empresa()
    {
        return $this->belongsTo(Employer::class, 'employer_id');
    }

    public function assign()
    {
        return $this->hasMany(AssignIssueUser::class, 'issue_id');
    }

    public function arquivos()
    {
        return $this->hasMany(IssueFile::class, 'issue_id');
    }

    public function message()
    {
        return $this->hasMany(Message::class, 'issue_id');
    }

    public function note()
    {
        return $this->hasMany(IssusNote::class, 'issue_id');
    }

}

<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Laravel\Scout\Searchable;


class Employer extends Model implements Transformable
{
    use TransformableTrait, Searchable;

    protected $fillable = [];

}

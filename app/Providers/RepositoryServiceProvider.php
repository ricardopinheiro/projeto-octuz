<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\EmpresaRepository::class, \App\Repositories\EmpresaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EmployerRepository::class, \App\Repositories\EmployerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoryRepository::class, \App\Repositories\CategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserAccountRepository::class, \App\Repositories\UserAccountRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IssueRepository::class, \App\Repositories\IssueRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AssignIssueUserRepository::class, \App\Repositories\AssignIssueUserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\MessageRepository::class, \App\Repositories\MessageRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IssueFileRepository::class, \App\Repositories\IssueFileRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IssusNoteRepository::class, \App\Repositories\IssusNoteRepositoryEloquent::class);
        //:end-bindings:
    }
}

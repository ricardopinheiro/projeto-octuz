<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface IssusNoteRepository
 * @package namespace App\Repositories;
 */
interface IssusNoteRepository extends RepositoryInterface
{
    //
}

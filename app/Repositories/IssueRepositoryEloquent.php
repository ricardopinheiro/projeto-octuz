<?php

namespace App\Repositories;

use App\Presenters\IssuePresenter;
use App\Transformers\IssueTransformer;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\IssueRepository;
use App\Entities\Issue;
use App\Validators\IssueValidator;

/**
 * Class IssueRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IssueRepositoryEloquent extends BaseRepository implements IssueRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Issue::class;
    }

    public function presenter()
    {
        return IssuePresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AssignIssueUserRepository
 * @package namespace App\Repositories;
 */
interface AssignIssueUserRepository extends RepositoryInterface
{
    //
}

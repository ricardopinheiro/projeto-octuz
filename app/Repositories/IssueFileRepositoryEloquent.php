<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\IssueFileRepository;
use App\Entities\IssueFile;
use App\Validators\IssueFileValidator;

/**
 * Class IssueFileRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IssueFileRepositoryEloquent extends BaseRepository implements IssueFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return IssueFile::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

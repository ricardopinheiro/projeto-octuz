<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\IssusNoteRepository;
use App\Entities\IssusNote;
use App\Validators\IssusNoteValidator;

/**
 * Class IssusNoteRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IssusNoteRepositoryEloquent extends BaseRepository implements IssusNoteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return IssusNote::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface IssueFileRepository
 * @package namespace App\Repositories;
 */
interface IssueFileRepository extends RepositoryInterface
{
    //
}

<?php

namespace App\Repositories;

use App\Presenters\AssignIssueUserPresenter;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AssignIssueUserRepository;
use App\Entities\AssignIssueUser;
use App\Validators\AssignIssueUserValidator;

/**
 * Class AssignIssueUserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AssignIssueUserRepositoryEloquent extends BaseRepository implements AssignIssueUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AssignIssueUser::class;
    }

    public function presenter()
    {
        return AssignIssueUserPresenter::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}

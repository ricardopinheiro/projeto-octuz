<?php

namespace App\Http\Controllers\Atendimento;

use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{


    public function login()
    {
        return view('atendimento.auth.login');
    }

    public function postLogin(Request $request){

        $data = $request->all();

        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        $user = User::where('email', $data['email'])->get();

        if(Hash::check($data['password'], $user->first()->password)){
            if($user->first()->role === 'admin'){
                Auth::loginUsingId($user->first()->id);
                return redirect('/admin/casos');
            }

            return back()->with('error', 'Você não tem permissão para acessar esta área');
        }

        return back()->with('error', 'Senha Incorreta');
    }
}

<?php

namespace App\Http\Controllers\Atendimento;

use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PipeController extends Controller
{

    public function index()
    {
        $users = User::where('role', 'admin')->get();
        return view("atendimento.casos")->with(compact('users'));
    }

}

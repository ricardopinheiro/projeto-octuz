<?php

namespace App\Http\Controllers\Painel;

use App\Entities\UserAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $account = UserAccount::find($id);

        $account->update($data);
        $account->save();
        return back()->with('success', 'Conta Alterada com Sucesso');

    }


    public function destroy($account)
    {
        $account = UserAccount::find($account)->delete();
        return back()->with('success', 'A Conta foi deletada');
    }
}

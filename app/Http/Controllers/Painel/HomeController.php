<?php

namespace App\Http\Controllers\Painel;

use App\Entities\Employer;
use App\Entities\Issue;
use App\Entities\IssueFile;
use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller
{

    public function index()
    {
        $casos = Issue::where('client_id', Auth::user()->id)->count();
        return view('painel.index')->with(compact('casos'));
    }

    public function perfil()
    {
        $casos = Issue::where('client_id', Auth::user()->id)->count();

        $resolvidos = Issue::where('client_id', Auth::user()->id)->where('issue_status', 3)->count();

        $reebolso = Issue::where('client_id', Auth::user()->id)->where('issue_status', 3)->sum('value');


        return view('painel.perfil')->with(compact('casos', 'resolvidos', 'reebolso'));
    }

    public function update(Request $request)
    {
        $data = $request->all();

        if($data['password'] !== '*******'){
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user = User::find(Auth::user()->id);
        $user->update($data);
        $user->save();

        return back()->with('success', 'Dados Atualizados com sucesso');
    }

    public function caso(Request $request)
    {
        $data = $request->all();

        $data['client_id'] = Auth::user()->id;
        $data['category_id'] = Employer::find($data['employer_id'])->category_id;

        $ssiue = Issue::create($data);

        if(isset($data['files'])){
            foreach ($data['files'] as $file) {
                IssueFile::create(['issue_id' => $ssiue->id, 'path_url' => URL::to("uploads/$file")]);
            }
        }

        return redirect()->back()->with('success', 'Caso adicionado com sucesso');
    }

}

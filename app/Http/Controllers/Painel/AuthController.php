<?php

namespace App\Http\Controllers\Painel;

use App\Entities\Employer;
use App\Entities\Issue;
use App\Entities\IssueFile;
use App\Entities\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $data = $request->all();

        $user = User::where('hash', $data['csrf'])->get();

        if($user->count() > 0){
            $user = $user->first();

            Auth::loginUsingId($user->id);

            $data['client_id'] = $user->id;
            $data['category_id'] = Employer::find($data['employer_id'])->category_id;

            $ssiue = Issue::create($data);

            if(isset($data['files'])){
                foreach ($data['files'] as $file) {
                    IssueFile::create(['issue_id' => $ssiue->id, 'path_url' => URL::to("uploads/$file")]);
                }
            }

            return redirect()->route('painel.index');
        }

        abort(404);
    }

}

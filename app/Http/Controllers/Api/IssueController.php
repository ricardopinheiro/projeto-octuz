<?php

namespace App\Http\Controllers\Api;

use App\Criteria\IssuesCriteria;
use App\Entities\Issue;
use App\Repositories\IssueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IssueController extends Controller
{
    /**
     * @var IssueRepository
     */
    private $issueRepository;

    /**
     * IssueController constructor.
     */
    public function __construct(IssueRepository $issueRepository)
    {
        $this->issueRepository = $issueRepository;
    }

    public function index(Request $request)
    {
        $this->issueRepository->pushCriteria(new IssuesCriteria($request->query()));
        $issues = $this->issueRepository->all();

        return ['success' => true, 'data' => $issues];
    }

    public function update(Request $request, $id)
    {
        $iss = Issue::find($id);
        $iss->update($request->all());
        $iss->save();

    }


    public function show($id)
    {
        $issue = $this->issueRepository->find($id);

        return ['success' => true, 'data'=>$issue];
    }
}

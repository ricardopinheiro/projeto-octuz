<?php

namespace App\Http\Controllers\Api;

use App\Repositories\MessageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IssueUserMessageController extends Controller
{
    /**
     * @var MessageUserRepository
     */
    private $messageUserRepository;


    /**
     * MessageUserController constructor.
     */
    public function __construct(MessageRepository $messageUserRepository)
    {
        $this->messageUserRepository = $messageUserRepository;
    }

    public function index($issue, $user)
    {

        $messages = $this->messageUserRepository->scopeQuery(function($query) use ($user, $issue){
            $query = $query->select('*', DB::raw("(IF (user_id = $user , true ,false)) as self"));
            return $query->where(['issue_id' => $issue]);
        })->all();

        return ['success' => true, 'data' => $messages];

    }

    public function store(Request $req, $issue, $user)
    {
        $data = $req->all();
        $data = $this->messageUserRepository->skipPresenter()->create(array_merge($data));

        $messages = $this->messageUserRepository->skipPresenter(false)->scopeQuery(function($query) use ($user, $issue, $data){
            $query = $query->select('*', DB::raw("(IF (user_id =$user , true ,false)) as self"));
            return $query->where('id', $data->id);
        })->all();

        return ['success' => true, 'data' => $messages[0]];

    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Entities\UserAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAccountController extends Controller
{

    public function show($user, $account)
    {

        $account = UserAccount::find($account);
        return ['success' => true, 'data' => $account];

    }

}

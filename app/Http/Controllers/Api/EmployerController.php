<?php

namespace App\Http\Controllers\Api;

use App\Entities\Employer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployerController extends Controller
{

    public function index(Request $request)
    {
        $se = $request->get('find');
        $res = Employer::search($se)->get();
        $arr = [];
        foreach ($res as $re) {
            array_push($arr, $re->name);
        }
        return $arr;
    }

    public function show($name)
    {
        $empresa = Employer::where('name', '=', $name)->get();

        if($empresa->count() > 0){
            $empresa = $empresa->first();
            return ['success' => true, 'data' => $empresa];
        }

        return ['success' => false, 'message' => 'Empresa não encontrada'];
    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Entities\AssignIssueUser;
use App\Entities\IssusNote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IssueNoteController extends Controller
{

    public function store(Request $request, $issue)
    {
        $data = $request->all();
        $data['issue_id'] = $issue;

        IssusNote::create($data);
    }

}

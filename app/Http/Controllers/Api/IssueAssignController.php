<?php

namespace App\Http\Controllers\Api;

use App\Entities\AssignIssueUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IssueAssignController extends Controller
{

    public function show($issue, $user)
    {
        $find = AssignIssueUser::where(['user_id' => $user, 'issue_id' => $issue])->count();

        if(!$find > 0){
            AssignIssueUser::create(['user_id' => $user, 'issue_id' => $issue]);
            return ['success' => true];
        }

        return ['success' => false];
    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Criteria\IssuesCriteria;
use App\Repositories\IssueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserIssueController extends Controller
{

    /**
     * @var IssueRepository
     */
    private $issueRepository;

    /**
     * UserIssueController constructor.
     */
    public function __construct(IssueRepository $issueRepository)
    {
        $this->issueRepository = $issueRepository;
    }

    public function index($user)
    {
        //$data = $this->issueRepository->findWhere(['client_id' => $user]);

        $data = $this->issueRepository->scopeQuery(function($query) use ($user){
            $query = $query->orderBy('id', 'desc');
            return $query->where(['client_id' => $user]);
        })->all();

        return ['success' => true, 'data' => $data];
    }


}

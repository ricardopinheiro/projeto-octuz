<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class IssuesCriteria
 * @package namespace App\Criteria;
 */
class IssuesCriteria implements CriteriaInterface
{
    /**
     * @var
     */
    private $query;

    /**
     * IssuesCriteria constructor.
     */
    public function __construct($query)
    {
        $this->query = $query;
    }


    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        if(isset($this->query['status']) && $this->query['status'] === 'all')
        {
            $model = $model->whereIn('issue_status', ['0','1']);
        }

        $model->orderBy('id', 'desc');

        if(isset($this->query['limit'])) {
            $model = $model->skip(0)->take($this->query['limit']);
        }

        return $model;
    }
}

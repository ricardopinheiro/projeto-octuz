<?php

namespace App\Presenters;

use App\Transformers\IssueFileTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class IssueFilePresenter
 *
 * @package namespace App\Presenters;
 */
class IssueFilePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new IssueFileTransformer();
    }
}

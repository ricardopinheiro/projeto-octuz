<?php

namespace App\Presenters;

use App\Transformers\MessageUserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class MessageUserPresenter
 *
 * @package namespace App\Presenters;
 */
class MessageUserPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MessageUserTransformer();
    }
}

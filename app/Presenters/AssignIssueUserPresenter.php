<?php

namespace App\Presenters;

use App\Transformers\AssignIssueUserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AssignIssueUserPresenter
 *
 * @package namespace App\Presenters;
 */
class AssignIssueUserPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AssignIssueUserTransformer();
    }
}

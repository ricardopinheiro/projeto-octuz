<?php

namespace App\Presenters;

use App\Transformers\UserAccountTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class UserAccountPresenter
 *
 * @package namespace App\Presenters;
 */
class UserAccountPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new UserAccountTransformer();
    }
}

<?php

namespace App\Presenters;

use App\Transformers\IssusNoteTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class IssusNotePresenter
 *
 * @package namespace App\Presenters;
 */
class IssusNotePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new IssusNoteTransformer();
    }
}

<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\IssueFile;

/**
 * Class IssueFileTransformer
 * @package namespace App\Transformers;
 */
class IssueFileTransformer extends TransformerAbstract
{

    /**
     * Transform the \IssueFile entity
     * @param \IssueFile $model
     *
     * @return array
     */
    public function transform(IssueFile $model)
    {
        return [
            'id'         => str_pad($model->id, 5, "0", STR_PAD_LEFT),
            'path_url'   => $model->path_url,
            'issue_id'   => $model->issue_id,
            'ext'        => pathinfo($model->path_url, PATHINFO_EXTENSION)
        ];
    }
}

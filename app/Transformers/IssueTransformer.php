<?php

namespace App\Transformers;

use App\Entities\MessageUser;
use League\Fractal\TransformerAbstract;
use App\Entities\Issue;
use App\Transformers\EmployerTransformer;

/**
 * Class IssueTransformer
 * @package namespace App\Transformers;
 */
class IssueTransformer extends TransformerAbstract
{

    protected $defaultIncludes = ['empresa', 'cliente', 'assigned', 'files', 'note'];

    /**
     * Transform the \Issue entity
     * @param \Issue $model
     *
     * @return array
     */
    public function transform(Issue $model)
    {
        return [
            'id'         => str_pad($model->id, 5, "0", STR_PAD_LEFT),
            'description' => $model->description,
            'solution_description' => $model->solution_description,
            'issue_status' => $model->issue_status,
            'status_color' => $this->getColor($model->issue_status),
            'status_title' => $this->getTitle($model->issue_status),
            'hint_problema' => $model->hint_problema,
            'hint_solucao' => $model->hint_solucao,
            'default_assign' => $model->assign->count() > 0 ? false : true,

            'created_at' => $model->created_at->format('d/m/Y H:i'),
            'updated_at' => $model->updated_at->format('d/m/Y H:i')
        ];
    }

    public function includeEmpresa(Issue $model)
    {
        $empresa = $model->empresa;
        return $this->item($empresa, new EmployerTransformer());
    }

    public function includeCliente(Issue $model)
    {
        $empresa = $model->cliente;
        return $this->item($empresa, new UserTransformer());
    }

    public function includeAssigned(Issue $model)
    {
        $assign = $model->assign;
        return $this->collection($assign, new AssignIssueUserTransformer());
    }

    public function includeFiles(Issue $model)
    {
        $assign = $model->arquivos;
        return $this->collection($assign, new IssueFileTransformer());
    }

    public function includeNote(Issue $model)
    {

        $notes = $model->note;
        return $this->collection($notes, new IssusNoteTransformer());
    }

    public function getColor($status)
    {
        switch($status){
            case 0:
                return 'default';
            case 1:
                return 'blue';
            case 2:
                return 'green';
            case 3:
                return 'yellow';
        }
    }

    public function getTitle($status)
    {
        switch($status){
            case 0:
                return 'Aguardando';
            case 1:
                return 'Em Resolução';
            case 2:
                return 'Resolvido';
            case 3:
                return 'Arquivado';
        }
    }
}

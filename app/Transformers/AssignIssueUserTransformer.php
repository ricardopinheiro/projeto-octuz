<?php

namespace App\Transformers;

use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\AssignIssueUser;

/**
 * Class AssignIssueUserTransformer
 * @package namespace App\Transformers;
 */
class AssignIssueUserTransformer extends TransformerAbstract
{

    /**
     * Transform the \AssignIssueUser entity
     * @param \AssignIssueUser $model
     *
     * @return array
     */
    public function transform(AssignIssueUser $model)
    {

        return [
            'id'         => (int) $model->id,
            'path_url'         => $model->user->path_url ?: URL::to('images/user.png'),
            'name'         => $model->user->name,
        ];
    }
}

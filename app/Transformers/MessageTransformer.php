<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Message;

/**
 * Class MessageTransformer
 * @package namespace App\Transformers;
 */
class MessageTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['user'];

    /**
     * Transform the \MessageUser entity
     * @param \MessageUser $model
     *
     * @return array
     */
    public function transform(Message $model)
    {
        return [
            'id'         => (int) $model->id,
            'message' => $model->message,
            'status' => $model->status,
            'user_id' => $model->user_id,
            'issue_id' => $model->issue_id,
            'self' => $model->self,

            'created_at' => $model->created_at->format('d') != date('d') ? $model->created_at->format('d/m/Y H:i') : $model->created_at->format('H:i'),
            'updated_at' => $model->updated_at->format('d/m/Y H:i:s')
        ];
    }

    public function includeUser(Message $model)
    {
        $user = $model->user;

        return $this->item($user, new UserTransformer());
    }
}

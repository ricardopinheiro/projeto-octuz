<?php

namespace App\Transformers;

use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\User;

/**
 * Class UserTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{

    /**
     * Transform the \User entity
     * @param \User $model
     *
     * @return array
     */
    public function transform(User $model)
    {

        return [
            'id'         => (int) $model->id,
            'name' => $model->name,
            'email' => $model->email,
            'cpf' => $model->cpf,
            'dt_nascimento' => $model->dt_nascimento,
            'path_url' => $model->path_url ?: URL::to('images/user.png'),
            'facebook_id' => $model->facebook_id,
            'fone' => $model->fone,
            'role' => $model->role,
            'address' => $model->address,
            'remember_token' => $model->remember_token,
        ];
    }
}

<?php

namespace App\Transformers;

use Illuminate\Support\Facades\URL;
use League\Fractal\TransformerAbstract;
use App\Entities\IssusNote;

/**
 * Class IssusNoteTransformer
 * @package namespace App\Transformers;
 */
class IssusNoteTransformer extends TransformerAbstract
{

    /**
     * Transform the \IssusNote entity
     * @param \IssusNote $model
     *
     * @return array
     */
    public function transform(IssusNote $model)
    {
        return [
            'id'         => (int) $model->id,
            'message'         => $model->message,
            'path_url'         => $model->user->path_url ?: URL::to('images/user.png'),
            'name'         => $model->user->name,
        ];
    }
}

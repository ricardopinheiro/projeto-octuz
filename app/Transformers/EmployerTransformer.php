<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Employer;

/**
 * Class EmployerTransformer
 * @package namespace App\Transformers;
 */
class EmployerTransformer extends TransformerAbstract
{

    /**
     * Transform the \Employer entity
     * @param \Employer $model
     *
     * @return array
     */
    public function transform(Employer $model)
    {
        return [
            'id'         => (int) $model->id,
            'name' => $model->name,
            'path_logo' => $model->path_logo,
            'path_cover' => $model->path_cover,
            'path_facebook' => $model->path_facebook,
            'path_twitter' => $model->path_twitter,
            'category_id' => $model->category_id,
        ];
    }
}

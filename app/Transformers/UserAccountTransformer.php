<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\UserAccount;

/**
 * Class UserAccountTransformer
 * @package namespace App\Transformers;
 */
class UserAccountTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserAccount entity
     * @param \UserAccount $model
     *
     * @return array
     */
    public function transform(UserAccount $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}

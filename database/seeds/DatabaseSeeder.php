<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        factory(\App\Entities\Category::class)->create([
            'name' => 'Aérea'
        ]);

        factory(\App\Entities\Category::class)->create([
            'name' => 'Telefônica'
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'Vivo',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/vivo-small.png'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/vivo-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'Tim',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/tim-small.jpg'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/tim-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'NET',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/net-small.png'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/net-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'Claro',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/claro-small.png'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/claro-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'Nextel',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/nextel-small.png'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/nextel-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'Copel',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/copel-small.png'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/copel-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);

        factory(\App\Entities\Employer::class)->create([
            'name' => 'Sky',
            'category_id' => 2,
            'path_logo' => \Illuminate\Support\Facades\URL::to('images/empresas/sky-small.png'),
            'path_cover' => \Illuminate\Support\Facades\URL::to('images/backgrounds/sky-bg.png'),
            'path_facebook' => '',
            'path_twitter' => '',
        ]);
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('password')->nullable();
            $table->string('cpf')->nullable();
            $table->string('address')->nullable();
            $table->string('dt_nascimento')->nullable();
            $table->string('hash')->nullable();
            $table->string('path_url')->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('fone')->nullable();
            $table->string('role', 10)->nullable()->default('user');
            $table->rememberToken();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}

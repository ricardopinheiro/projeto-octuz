@extends('painel.template')

@section('content')
    <div class="app-content content container-fluid" style="margin-left: inherit;">
        <div class="content-wrapper">
            <div class="content-body col-md-12 col-lg-8 offset-lg-2" style="padding: 25px;">
                <div class="content-i">
                    <div class="content-box">
                        <div class="row">
                            <div class="col-sm-12 col-md-6">
                                <div class="user-profile compact">
                                    <div class="up-head-w" style="text-align: center;vertical-align: middle;padding: 70px;background: #018acd;color:white">
                                        <div class="value">
                                            <i class="ti-settings" style="font-size: 5rem;color: white;text-align: center;"></i>
                                            <div class="up-main-info">
                                                <h2 class="up-header">MINHA CONTA</h2>
                                            </div>
                                        </div>
                                        <svg class="decor" width="842px" height="219px" viewBox="0 0 842 219" preserveAspectRatio="xMaxYMax meet" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g transform="translate(-381.000000, -362.000000)" fill="#FFFFFF"><path class="decor-path" d="M1223,362 L1223,581 L381,581 C868.912802,575.666667 1149.57947,502.666667 1223,362 Z"></path></g></svg>
                                    </div>
                                    <div class="up-controls">
                                        <div class="row">
                                            <div class="col-sm-12 text-right">
                                                <a class="btn btn-sm btn-f pull-right" href="">
                                                    <i class="icon-social-facebook"></i>
                                                    <span>Facebook</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="up-contents">
                                        <div class="m-b">
                                            <div class="row m-b">
                                                <div class="col-sm-4 b-r b-b">
                                                    <div class="el-tablo centered padded-v">
                                                        <div class="value">
                                                            <i class="ti-comment-alt"></i>
                                                        </div>
                                                        <div class="value" style="font-size: 1.8rem;">
                                                            {{ $casos }}
                                                        </div>
                                                        <div class="label">CASOS</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 b-b">
                                                    <div class="el-tablo centered padded-v">
                                                        <div class="value">
                                                            <i class="icon-check"></i>
                                                        </div>
                                                        <div class="value" style="font-size: 1.8rem;">
                                                            {{ $resolvidos }}
                                                        </div>
                                                        <div class="label">RESOLVIDOS</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 b-b">
                                                    <div class="el-tablo centered padded-v">
                                                        <div class="value">
                                                            <i class="ti-money"></i>
                                                        </div>
                                                        <div class="value" style="font-size: 1.8rem;">
                                                            R$ {{ $reebolso }}
                                                        </div>
                                                        <div class="label">RECUPERADOS</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row m-b">
                                                <h6 class="element-header">Minhas Contas</h6>
                                                <div class="col-md-12">
                                                    @if(Auth::user()->account->count() == 0)
                                                        <div class="el-tablo centered padded-v">
                                                            <div class="value">
                                                                <i class="ti-id-badge"></i>
                                                            </div>
                                                            <div class="label">Nenhuma conta cadastrada</div>
                                                        </div>
                                                    @endif
                                                    <div class="table-responsive">
                                                        <table class="table table-lightborder text-left">

                                                            <tbody>
                                                                @foreach(Auth::user()->account as $account)
                                                                <tr>
                                                                    <td class="nowrap">
                                                                        <div class="cell-image-list">
                                                                            <div class="cell-img" title="{!! $account->empresa->name !!}" style="background-image: url({!! $account->empresa->path_logo !!})"></div>
                                                                        </div>
                                                                    </td>
                                                                    <td class="nowrap">{{ $account->login ?: '' }}</td>
                                                                    <td class="text-right">{{ $account->numero_conta }}</td>
                                                                    <td class="text-right" data-id="{!! $account->id !!}">
                                                                        <a href="#" onclick="editAccount(this)" data-id="{!! $account->id !!}">
                                                                            <i class="ti-pencil-alt" ></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="element-wrapper">
                                    <div class="element-box">
                                        <form id="formValidate" action="/painel/perfil" method="POST" novalidate="true">
                                            <div class="element-info">
                                                <div class="element-info-with-icon">
                                                    <div class="element-info-icon">
                                                        <div class="ti-user"></div>
                                                    </div>
                                                    <div class="element-info-text">
                                                        <h5 class="element-inner-header">Informações Pessoais</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for=""> Nome</label>
                                                <input class="form-control"  name="name" placeholder="Nome" required="required" readonly type="text" value="{{ Auth::user()->name }}">
                                            </div>
                                            <div class="form-group">
                                                <label for=""> Email</label>
                                                <input class="form-control"  name="email" placeholder="E-mail" required="required" readonly type="email" value="{{ Auth::user()->email }}">
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for=""> Telefone</label>
                                                        <input class="form-control"  name="fone" id="fone" placeholder="Telefone" readonly required="required" type="text" value="{{ Auth::user()->fone }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for=""> CPF</label>
                                                        <input class="form-control"  name="cpf" placeholder="CPF" id="cpf" readonly required="required" type="text" value="{{ Auth::user()->cpf }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for=""> Data de Nascimento</label>
                                                        <input class="form-control"  name="dt_nascimento" id="data" placeholder="Data de nascimento" readonly required="required" type="text" value="{{ Auth::user()->dt_nascimento }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for=""> Senha</label>
                                                        <input class="form-control" name="password"  placeholder="Senha" value="*******" readonly required="required" type="password">
                                                    </div>
                                                </div>
                                            </div>
                                            <fieldset class="form-group">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for=""> Endereço</label>
                                                            <input class="form-control" name="address" placeholder="Endereço Completo" required="required" readonly type="text" value="{{ Auth::user()->address }}">
                                                            <div class="help-block form-text with-errors form-control-feedback"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <div class="form-buttons-w">
                                                <div class="row">
                                                    <div class="col-md-12 text-center">
                                                        <button class="btn btn-default btn-unlock" type="button">Editar</button>
                                                        <button class="btn btn-primary btn-submit-f" disabled type="submit">Salvar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade text-xs-left" id="iconForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel34" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h3 class="modal-title" id="myModalLabel34">Dados da Conta</h3>
                </div>
                <form action="" method="post" id="form-del">
                    <input type="hidden" name="_method" value="DELETE">
                </form>
                <form action="" method="POST" id="form-acc">
                    <input type="hidden" name="id" id="id_conta">
                    <div class="modal-body">
                        <label>Telefone: </label>
                        <div class="form-group position-relative has-icon-left">
                            <input placeholder="Email Address" name="numero_conta" class="form-control" id="telefone_modal" type="text">
                            <div class="form-control-position">
                                <i class="fa fa-phone font-medium-5 line-height-1 text-muted icon-align"></i>
                            </div>
                        </div>

                        <label>Login: </label>
                        <div class="form-group position-relative has-icon-left">
                            <input placeholder="Email Address" name="login" class="form-control" id="login_modal" type="text">
                            <div class="form-control-position">
                                <i class="fa fa-user font-medium-5 line-height-1 text-muted icon-align"></i>
                            </div>
                        </div>

                        <label>Senha: </label>
                        <div class="form-group position-relative has-icon-left">
                            <input placeholder="Password" name="senha" class="form-control" id="senha_modal" type="text">
                            <div class="form-control-position">
                                <i class="fa fa-lock font-large-1 line-height-1 text-muted icon-align"></i>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input class="btn btn-outline-danger btn-lg pull-left" onclick="$('#form-del').submit()" value="Remover" type="reset">
                        <input class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar" type="reset">
                        <input class="btn btn-outline-primary btn-lg" value="Salvar" type="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Stack admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, stack admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Octuz | Painel</title>
    <link rel="apple-touch-icon" href="{{ asset("images/favicon.ico") }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset("images/favicon.ico") }}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/feather/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/extensions/pace.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/charts/jquery-jvectormap-2.0.3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/extensions/unslider.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/weather-icons/climacons.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/app.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/themify-icons.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/menu/menu-types/vertical-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/os.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/style.css') }}">

    <script>
        (function(h,e,a,t,m,p) {
            m=e.createElement(a);m.async=!0;m.src=t;
            p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
        })(window,document,'script','https://u.heatmap.it/log.js');
    </script>

    <script type="text/javascript">
        window.smartlook||(function(d) {
            var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
            var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
            c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
        })(document);
        smartlook('init', 'b252a61dec3e0b28906cf8ab56309467a9fd615b');
    </script>

    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1698839770440223');
        fbq('track', 'PageView');
        fbq('track', 'Lead');
        fbq('track', 'CompleteRegistration');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1698839770440223&ev=PageView&noscript=1"
        /></noscript>

</head>
<body data-open="click" data-id="{{ Auth::user()->id }}" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns  fixed-navbar">

<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-dark bg-fisher navbar-shadow navbar-brand-center" data-nav="brand-center">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item mobile-menu hidden-md-up float-xs-left">
                    <a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a>
                </li>
                <li class="nav-item">
                    <a href="/painel" class="navbar-brand">
                        <img alt="stack admin logo" src="{{ asset("images/logo.png") }}" class="brand-logo">
                    </a>
                </li>
                <li class="nav-item hidden-md-up float-xs-right">
                    <a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container">
                        <i class="fa fa-ellipsis-v"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                <ul class="nav navbar-nav float-xs-right">
                    <li class="dropdown dropdown-user nav-item" style="width: 100%;">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link">
                            <span class="avatar avatar-online">
                                <img src="{{ Auth::user()->path_url ?: URL::to('images/user.png') }}" class="avatar-img" alt="avatar">
                                <i></i>
                            </span>
                            <span class="user-name">{{ Auth::user()->name }}</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <button onclick="$('#modalTelefonica').modal('toggle')" type="button" class="dropdown-item">
                                <i class="ti-pencil"></i>
                                Novo Caso
                            </button>
                            <a href="/painel" class="dropdown-item">
                                <i class="ti-layers-alt"></i>
                                Meus Casos
                            </a>
                            <a href="/painel/perfil" class="dropdown-item">
                                <i class="ft-user"></i>
                                Editar Perfil
                            </a>
                            <div class="dropdown-divider"></div>
                            <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

@yield('nav')

@yield('content')


<script src="{{ asset('user/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/extensions/jquery.knob.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/scripts/extensions/knob.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/morris.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('landing/js/typehead.js') }}"></script>
<script src="{{ asset('user/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('user/data/jvector/visitor-data.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/extensions/unslider-min.js') }}" type="text/javascript"></script>
<link href="{{ asset('user/css/core/colors/palette-climacon.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('user/fonts/simple-line-icons/style.min.css') }}" rel="stylesheet" type="text/css" >
<script src="{{ asset('user/js/core/app-menu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/core/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/scripts/pages/dashboard-analytics.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/jquery-bootstrap-modal-steps.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="https://cdn.firebase.com/js/client/2.0.4/firebase.js"></script>
<script src="{{ asset('user/js/jquery.mask.min.js') }}"></script>
<script src="{{ asset('user/js/octuz.js') }}" type="text/javascript"></script>
@yield('css')
@if(Session::has('success'))
    <script>
        toastr.success('{!! Session::get('success') !!}')
    </script>
@endif
</body>
</html>
@extends('painel.template')

@section('nav')
    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-color-overlay menu-accordion menu-shadow bg-fisher">
        <div class="main-menu-content bg-fisher-inverse">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main issues"></ul>
        </div>
    </div>
@endsection

@section('content')
    <div class="app-content content container-fluid">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="social-cards"></section>
                @if($casos <= 0)
                    <div class="row">
                        <div style="margin-top: 50px;text-align: center;" class="col-md-3 offset-md-3 m-t-50">
                            <i class="ti-face-sad" style="font-size: 6.9rem;margin-top: 25px;padding: 50px;"></i>
                            <br><br>
                            <h4>Você ainda não possui nenhum caso cadastrado</h4>
                           <br><br>
                            <button type="button" onclick="$('#modalTelefonica').modal('toggle')" class="btn btn-outline-primary btn-block">
                                <i class="ti-pencil-alt"></i> Cadastrar
                            </button>
                        </div>
                    </div>
                @endif
                @include('painel.modal')

            </div>
        </div>
    </div>


@endsection

@section('css')
    @if($casos <= 0)
    <style>
        body.vertical-layout.vertical-menu.menu-expanded .main-menu {
            width:0;
        }
    </style>

    @endif

    <script>
        $("#modalTelefonica").modalSteps()

        $('#radioBtn a').on('click', function(){
            var sel = $(this).data('title');
            var tog = $(this).data('toggle');
            $('#'+tog).prop('value', sel);

            $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
            $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
        })


        $('.upload-btn').on('click', function (){
            $('#upload-input').click();
            $('.progress-bar').text('0%');
            $('.progress-bar').width('0%');
        });

        $('#upload-input').on('change', function(){

            var files = $(this).get(0).files;

            if (files.length > 0){
                var formData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    formData.append('uploads[]', file, file.name);
                }

                $.ajax({
                    url: '/upload',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data){
                        $.each(data.data, function (i, v){
                            $("#recipes").append('<input type="hidden" id="files" name="files[]" value="'+v+'">')
                        })
                    },
                    xhr: function() {
                        var xhr = new XMLHttpRequest();

                        xhr.upload.addEventListener('progress', function(evt) {

                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('.progress-bar').text(percentComplete + '%');
                                $('.progress-bar').width(percentComplete + '%');
                                if (percentComplete === 100) {
                                    $('.progress-bar').html('100%');
                                }
                            }

                        }, false);

                        return xhr;
                    }
                });
            }
        });

        $(document).ready(function(){
            $.get("https://octuz.com/api/empresa?find=", function(data){
                $(".typehead").typeahead({
                    display: 'nome',
                    source:data
                });
            },'json');
        })

    </script>
    <style>
        .modal-body {
            position: relative;
            padding: 0px 15px;
        }
    </style>
@endsection


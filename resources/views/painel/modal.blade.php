<div class="modal fade" id="modalTelefonica" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12" style="padding: 5px">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="close close-popup ripe" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                        </div>
                    </div>
                </div>
                <form action="/painel/cadastrar/caso" method="POST" id="form-telefonica">
                    <div class="row hide" data-step="1" data-wow-delay='.5s'>
                        <div class="well fadeIn" >
                            <div class="row">
                                <div class="col-md-12 text-center" style="padding: 25px;">
                                    <h4>Qual é o problema que deseja solucionar? </h4>
                                    <p>Por favor, conte-nos o que está acontecendo</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 err-pb hidden" style="margin-top: 10px">
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <strong>Atenção!</strong> Selecione uma opção
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:25px;">
                                    <div class="form-group">
                                        <input class="form-control typehead" id="mc-empresa" autocomplete="off" placeholder="Digite o nome da Empresa..." required="required" name="empresa" type="text">
                                        <input type="hidden" id="employer_id" name="employer_id">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" style="padding:15px;" class="btn btn-info btn-block js-btn-step" data-orientation="next">Avançar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide" data-step="2" data-wow-delay='.5s'>
                        <div class="well fadeIn" >
                            <div class="row">
                                <div class="col-md-12 text-center" style="padding: 25px;">
                                    <h4>Qual é o problema que deseja solucionar? </h4>
                                    <p>Por favor, conte-nos o que está acontecendo</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 err-pb hidden" style="margin-top: 10px">
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <strong>Atenção!</strong> Selecione uma opção
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group" style="width: 100%;">
                                        <div id="radioBtn" class="btn-group" style="width: 100%;">
                                            <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_problema" data-title="Funcionamento">Funcionamento</a></div>
                                            <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_problema" data-title="Cobrança">Cobrança</a>
                                            </div>
                                            <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_problema" data-title="Outros">Outros</a>
                                            </div>
                                        </div>
                                        <input type="hidden" name="hint_problema" id="hint_problema" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-12 err-desc-pb hidden" style="margin-top: 10px">
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <strong>Atenção!</strong> Adicone uma desrição
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:25px;">
                                    <div class="form-group">
                                        <textarea name="description" placeholder="Conte-nos o que está acontecendo" autocomplete="off" id="problema_descricao" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="recipes" style="margin-top:55px;">
                                    <input id="upload-input" type="file" name="uploads[]" multiple="multiple">
                                    <a href="#" class="upload-btn" style="font-size:18px;color:#32a6c9;"><i class="ti-clip"></i> Anexar arquivo</a>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" style="padding:15px;" class="btn btn-info btn-block js-btn-step" data-orientation="next">Avançar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row hide" data-step="3" data-title="This is the second and last step!">
                        <div class="well fadeIn" data-wow-delay='.5s'>
                            <div class="row">
                                <div class="col-md-12 text-center" style="padding: 25px;">
                                    <h4>Qual é a solução que você espera?</h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 err-sl hidden" style="margin-top: 10px">
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <strong>Atenção!</strong> Selecione uma opção
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="input-group" style="width: 100%;">
                                        <div id="radioBtn" class="btn-group" style="width: 100%;">
                                            <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_solucao" data-title="Reembolso">Reembolso</a></div>
                                            <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_solucao" data-title="Cancelamento">Cancelamento</a>
                                            </div>
                                            <div class="col-md-4 col-xs-12" style="margin-bottom:5px;">
                                                <a class="btn btn-info btn-overlay btn-sm notActive" data-toggle="hint_solucao" data-title="Outros">Outros</a>
                                            </div>
                                        </div>
                                        <input type="hidden" autocomplete="off" name="hint_solucao" id="hint_solucao">
                                    </div>
                                </div>
                                <div class="col-md-12 err-desc-sl hidden" style="margin-top: 10px">
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <strong>Atenção!</strong> Adicone uma desrição
                                    </div>
                                </div>
                                <div class="col-md-12" style="margin-top:25px;">
                                    <div class="form-group">
                                        <textarea autocomplete="off" placeholder="Que solução você espera?" name="solution_description" id="solucao_descricao" class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <button type="{{ $casos <= 0 ? 'button' : 'submit' }}" style="padding:15px;" class="btn btn-info btn-block js-btn-step" data-orientation="next">Avançar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($casos <= 0)
                    <div class="row hide" data-step="4" data-title="This is the second and last step!">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-12 text-center" style="padding: 25px;">
                                    <h4>Dados do titular</h4>
                                    <p>Seus dados estão seguros, e serão utilizados apenas para conﬁrmações com a empresa em questão.</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="error-updater">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Nome completo</label>
                                            <input autocomplete="off" type="text" class="form-control i-octuz" id="name_upd" name="name_upd"  aria-describedby="emailHelp" placeholder="Nome do titular">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="address">Endereço completo</label>
                                            <input autocomplete="off" type="text" class="form-control i-octuz" id="address_upd" name="address_upd"  aria-describedby="emailHelp" placeholder="Endereço completo">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="fone">Telefone com problema</label>
                                            <input autocomplete="off" type="text" class="form-control i-octuz" id="fone_upd" name="fone_upd"  aria-describedby="emailHelp" placeholder="Telefone com problema">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="contact_fone">Telefone para contato</label>
                                            <input autocomplete="off" type="text" class="form-control i-octuz" id="contact_fone_upd" name="contact_fone_upd"  aria-describedby="emailHelp" placeholder="Telefone para contato">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="dt_nascimento">Data de Nascimento</label>
                                            <input autocomplete="off" type="text" class="form-control i-octuz" id="dt_nascimento_upd" name="dt_nascimento_upd"  aria-describedby="emailHelp" placeholder="Data de Nascimento">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cpf">CPF</label>
                                            <input autocomplete="off" type="text" class="form-control i-octuz" id="cpf_upd"  name="cpf_upd"  aria-describedby="emailHelp" placeholder="CPF do titular">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" style="padding:15px;" class="btn btn-success btn-block btn-update-data">Avançar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
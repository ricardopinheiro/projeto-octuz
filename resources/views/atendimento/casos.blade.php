@extends('atendimento.template')

@section('content')

    @include('atendimento.fila')



    <div class="row">
        <div class="col-md-12">
            <div class="content-i">
                <div class="content-box">
                    <div class="row">
                        @include('atendimento.empresa')

                        @include("atendimento.chat")
                    </div>
                </div>

                <div class="content-panel" style="padding: 2rem 0;">
                    @include('atendimento.user')
                </div>
            </div>
        </div>
    </div>

@endsection

@section('modal')
    <div class="modal fade text-xs-left" id="modalAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <label class="modal-title text-text-bold-600" id="myModalLabel33">Selecionar atendente</label>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <select class="select2-placeholder form-control" id="single-placeholder" style="width: 100%">
                               @foreach($users as $user)
                                    <option value="{{ $user->id }}">{{ $user->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-outline-secondary btn-lg" data-dismiss="modal" value="Cancelar" type="reset">
                    <input class="btn btn-outline-primary btn-lg btn-assign" value="Adicionar" type="submit">
                </div>
            </div>
        </div>
    </div>
@endsection
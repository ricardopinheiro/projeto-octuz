<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>Octuz Atendimento</title>
    <link rel="shortcut icon" type="image/x-icon" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/fonts/feather/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/vendors/css/extensions/pace.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/app.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/core/menu/menu-types/vertical-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/vendors/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/chat-application.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('atendimento/css/style.css') }}">
</head>
<body data-open="click" data-id="{{ Auth::user()->id }}" data-menu="vertical-menu" data-col="content-left-sidebar" class="vertical-layout vertical-menu content-left-sidebar chat-application  menu-collapsed fixed-navbar">

<nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-dark bg-white navbar-shadow navbar-brand-center" data-nav="brand-center">
    <div class="navbar-wrapper">
        <div class="navbar-header">
            <ul class="nav navbar-nav">
                <li class="nav-item">
                    <a href="index.html" class="navbar-brand">

                        <img alt="admin logo" style="max-width: 110px" src="{{ asset('images/logo_color.png') }}" class="brand-logo">

                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-container content container-fluid">
            <div id="navbar-mobile" class="collapse navbar-toggleable-sm">
                <ul class="nav navbar-nav float-xs-right">
                    <li class="dropdown dropdown-user nav-item">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle nav-link dropdown-user-link" style="color: black;padding: 1.4rem .6rem;">
                            <span class="avatar avatar-online">
                                <img src="{{ Auth::user()->path_url ?: URL::to('images/user.png') }}" style="height: 30px;" alt="avatar"><i></i></span><span class="user-name">{{ Auth::user()->name }}</span></a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-divider"></div><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item"><i class="ft-power"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
{{--

<div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
            <li class="navigation-header"><span>Geral</span><i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus"></i>
            </li>
            <li class="nav-item">
                <a href="/admin">
                    <i class="ft-home"></i>
                    <span data-i18n="" class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/users">
                    <i class="ft-home"></i>
                    <span data-i18n="" class="menu-title">Usuários</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/admin/casos">
                    <i class="ft-home"></i>
                    <span data-i18n="" class="menu-title">Casos</span>
                </a>
            </li>
        </ul>
    </div>
</div>
--}}

<div class="app-content content container-fluid">
   @yield('content')
</div>

<script src="{{ asset('atendimento/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('atendimento/js/core/app-menu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('atendimento/js/core/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('atendimento/js/scripts/customizer.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('atendimento/js/scripts/pages/chat-application.js') }}" type="text/javascript"></script>
<script src="{{ asset('atendimento/js/scripts/select2.full.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" type="text/javascript"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="https://cdn.firebase.com/js/client/2.0.4/firebase.js"></script>

@yield('modal')

<script>

  function send() {
    var message = $("#message-chat").val();
    var issue = $("#issue-id").val(iss);
    var user = $("body").data('id');
    var avatar = ($(".avatar-img").length > 0) ? $(".avatar-img").attr('src') : '{{ URL::to('images/user.png') }}'

    $.get('/templates/chat.mst', function(tpl) {
      var data = {
        message: message,
        user: {
          path_url: avatar,
        },
        created_at: new Date(),
        self: true
      }

      $(".chat-content").append(Mustache.to_html(tpl, data));
    });
    submitMessage()
  }

  function submitMessage()
  {
    var message = $("#message-chat").val();
    var issue = $("#issue-id").val();
    var user = $("body").data('id');

    $.post( "/api/issue/"+issue+"/user/"+user+"/messages",
      { message: message, user_id: user, issue_id: issue, status: 0 },
      function( ) {
        console.log('ok')
    });
    $("#message-chat").val('');
  }

  $.get('/templates/atendimento.mst', function(tpl) {
    $.get("/api/issue?status=all&limit=3")
      .done(function( data ) {
        $.each(data.data, function(key,val) {
          $("#fila-atendimento").append(Mustache.to_html(tpl, val));
        });
      });
  });

  function loadDataIssue(iss) {
    $("#issue-id").val(iss);
    getDataIssue(iss)
  }

  function getDataIssue() {
    $("#issue-data").empty();
    $.get('/templates/empresa.mst', function(tpl) {
      iss =  $("#issue-id").val();
      getDataChat()
      $.get("/api/issue/"+iss)
        .done(function( data ) {
          $("#issue-data").append(Mustache.to_html(tpl, data.data));
        });
    });
  }

  function getDataChat(){
    $("#element-chat").empty()
    $.get('/templates/body.mst', function(tpl) {
      $("#element-chat").append(Mustache.to_html(tpl, {}));
    })

    $(".chat-content").empty();
    $.get('/templates/chat.mst', function(tpl) {
      var issue = $("#issue-id").val();
        getDataUser();
      firebaseListen();
      var user = {{ Auth::user()->id }};
      $.get("/api/issue/"+issue+"/user/"+user+"/messages")
        .done(function( data ) {
          $.each(data.data, function(key,val) {
            $(".chat-content").append(Mustache.to_html(tpl, val));
          });

          $(".ps").scrollTop( $(".chat-content").prop( "scrollHeight" ));
          $(".ps").perfectScrollbar('update');
        });
    });
  }

  function getDataUser() {
      $("#user-element").empty()
    $.get('/templates/user.mst', function(tpl) {
      var issue = $("#issue-id").val();
      $.get("/api/issue/"+issue)
        .done(function( data ) {
          $("#user-element").append(Mustache.to_html(tpl, data.data));
        }).done(function(){
          getNotes()
      });
    });
  }

  function getNotes()
  {
      $.get('/templates/note.mst', function(tpl) {
          var issue = $("#issue-id").val();
          $.get("/api/issue/"+issue)
              .done(function( data ) {
                  $("#notes").append(Mustache.to_html(tpl, data.data));
              });
      });
  }

  function firebaseListen() {
    var issue = $("#issue-id").val();
    var messagesRef = new Firebase('{{ config('services.firebase.database_url') }}/message_users/'+issue+'/');
    console.log('{{ config('services.firebase.database_url') }}/message_users/'+issue+'/');
    messagesRef.limitToLast(10).on('child_added', function (snapshot) {
      var data = snapshot.val();

      $.get('/templates/chat.mst', function(tpl) {
        var user = {{ Auth::user()->id }};
        data.self = user === data.user.id ? true : false;
        if(data.self === false){
          $(".chat-content").append(Mustache.to_html(tpl, data));
          $(".ps").scrollTop( $(".chat-content").prop( "scrollHeight" )+100 );
          $(".ps").perfectScrollbar('update');
        }
      });
    });

  }

  function addAssign(id){
      $("#single-placeholder").select2()
      $("#modalAssign").modal('toggle')
  }

  $('.btn-assign').on('click', function(){
      var issue = $("#issue-id").val();
      var user =  $("#single-placeholder").val();
      $.get( "/api/issue/"+issue+"/assign/"+user+"", function(data){
          if(data.success === true){
              toastr.success('Adicionado com sucesso')
          } else {
              toastr.success('Usuário já está adicionado')
          }

          $("#modalAssign").modal('toggle')
      });
  })

    function setStatus (el){
        var issue = $("#issue-id").val();
        var status = $(el).data('status')
        $.post( "/api/issue/"+issue,
            { issue_status: status, _method: 'PATCH' },
            function( ) {
                toastr.success('Status Alterado com sucesso')
            });
    }

   function sendNote(){
        var issue = $("#issue-id").val();
        var user =  $("#single-placeholder").val();
        var message = $("#message-aux").val();

        $.post( "/api/issue/"+issue+'/note',
            { user_id: user, issue_id: issue, message:message},
            function( ) {
                toastr.success('Anotação adicionada com sucesso')
            }).done(function(){
            getDataUser();
        });
    }

</script>
</body>

</html>

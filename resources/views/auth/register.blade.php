<!DOCTYPE html>
<html lang="pt-br" data-textdirection="ltr" class="loading">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cadastro | Octuz</title>
    <meta name="description" content="A Octuz é uma plataforma confiável que administra e resolve problemas com empresas. Seja um atendimento mal resolvido com uma operadora, um voo cancelado de uma companhia área, a Octuz conecta você com a sua tranquilidade por meio de um atendimento personalizado, rápido e seguro.">
    <meta name="keywords" content="vivo, tim, octuz, procon, reclamação sac, anatel, net, claro, lei do consumidor, resolver problema, procon online">
    <meta http-equiv="content-language" content="pt-br">
    <meta name="author" content="Octuz">

    <title>Octuz</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/feather/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/extensions/pace.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/app.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/menu/menu-types/vertical-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/pages/login-register.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/style.css') }}">

</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column  blank-page blank-page">
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
                <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                    <div class="card border-grey border-lighten-3 px-2 py-2 m-0">
                        <div class="card-header no-border">
                            <div class="card-title text-xs-center">
                                <img src="{{ asset('images/logo_color.png') }}" style="max-width:120px;" alt="logo octuz">
                            </div>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block">
                                <form class="form-horizontal form-simple" method="POST" action="{{ route('register') }}">
                                    {{ csrf_field() }}

                                <form class="form-horizontal form-simple" method="POST" action="{{ route('register') }}" novalidate>
                                    <fieldset class="form-group position-relative has-icon-left mb-1 {{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input type="text" name="name" class="form-control form-control-lg input-lg" id="user-name" value="{{ old('name') }}" placeholder="Nome completo">
                                        <div class="form-control-position">
                                            <i class="ft-user"></i>
                                        </div>
                                        @if ($errors->has('name'))
                                            <p class="text-xs-right"><small class="danger text-muted">{{ $errors->first('name') }}</small></p>
                                        @endif
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left mb-1 {{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <input type="email" name="email" class="form-control form-control-lg input-lg" id="user-email" name="email" value="{{ old('email') }}" placeholder="Endereço de e-mail" required>
                                        <div class="form-control-position">
                                            <i class="ft-mail"></i>
                                        </div>
                                        @if ($errors->has('email'))
                                            <p class="text-xs-right"><small class="danger text-muted">{{ $errors->first('email') }}</small></p>

                                        @endif
                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left mb-1 {{ $errors->has('password') ? ' has-danger' : '' }}" >
                                        <input type="password" name="password" class="form-control form-control-lg input-lg" id="user-password" style="margin-bottom: 0rem;" placeholder="Digite sua senha" required>
                                        <div class="form-control-position">
                                            <i class="fa fa-key"></i>
                                        </div>
                                        @if ($errors->has('password'))
                                            <p class="text-xs-right"><small class="danger text-muted">{{ $errors->first('password') }}</small></p>
                                        @endif

                                    </fieldset>
                                    <fieldset class="form-group position-relative has-icon-left">
                                        <input type="password" name="password_confirmation" class="form-control form-control-lg input-lg" id="user-password" placeholder="Confirme sua senha" required>
                                        <div class="form-control-position">
                                            <i class="fa fa-key"></i>
                                        </div>
                                    </fieldset>

                                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="ft-unlock"></i> Cadastrar</button>
                                </form>
                            </div>
                            <p class="text-xs-center">Já possui uma conta? <a href="/login" class="card-link">Login</a></p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<script src="{{ asset('user/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/forms/validation/jqBootstrapValidation.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/core/app-menu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/core/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/scripts/customizer.min.js') }}" type="text/javascript"></script>

<script src="{{ asset('user/js/scripts/forms/form-login-register.min.js') }}" type="text/javascript"></script>
<script>
    (function(h,e,a,t,m,p) {
        m=e.createElement(a);m.async=!0;m.src=t;
        p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
    })(window,document,'script','https://u.heatmap.it/log.js');
</script>

<script type="text/javascript">
    window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'b252a61dec3e0b28906cf8ab56309467a9fd615b');
</script>

<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1698839770440223');
    fbq('track', 'PageView');
    fbq('track', 'Lead');
    fbq('track', 'CompleteRegistration');
</script>
</body>

</html>
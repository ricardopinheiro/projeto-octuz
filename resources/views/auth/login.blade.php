<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login | Octuz.</title>
    <meta name="description" content="A gente deixa você curtir a vida enquanto resolvemos seus problemas com empresas, SACs, Fale Conosco, 0800, música de espera">
    <meta name="keywords" content="anatel,resolver problemas,reclame aqui,problemas com operadoras,problemas com a net,combo multi,cobrança indevida,atraso na entrega,proteste,procon,consumidor,dividas bancarias,cobrança indevida operadora,cobranças net,cobranças vivo,cobrança claro,problemas claro,problemas gvt,problemas oi,net,vivo,oi,gvt,tim,claro,creditos tim,problemas tim">
    <meta http-equiv="content-language" content="pt-br">
    <meta name="author" content="Octuz">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/feather/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/extensions/pace.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/charts/jquery-jvectormap-2.0.3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/charts/morris.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/extensions/unslider.css') }}">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/vendors/css/weather-icons/climacons.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/app.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/fonts/simple-line-icons/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/themify-icons.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/menu/menu-types/vertical-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/menu/menu-types/vertical-overlay-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/core/colors/palette-gradient.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/os.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/style.css') }}">



</head>
<body data-open="click" data-menu="vertical-menu" data-col="1-column" class="vertical-layout vertical-menu 1-column bg-cyan bg-lighten-2 blank-page blank-page">

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
            <section class="flexbox-container">
                <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
                    <div class="card border-grey border-lighten-3 m-0">
                        <div class="card-header no-border">
                            <div class="card-title text-xs-center">
                                <div class="p-1"><img src="{{ asset('images/logo_color.png') }}" style="max-width: 180px;" alt="branding logo"></div>
                            </div>
                            <h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2">
                                <span>Entrar</span>
                            </h6>
                        </div>
                        <div class="card-body collapse in">
                            <div class="card-block pt-0">
                                <form class="form-horizontal" id="form-login" action="/login" method="post">
                                    <fieldset class="form-group floating-label-form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                                        <label for="email">E-mail</label>
                                        <input type="text"  required class="form-control {{ $errors->has('password') ? ' form-control-danger' : '' }}" id="email" name="email" placeholder="E-mail">
                                        @if ($errors->has('email'))
                                            <p class="text-xs-right"><small class="danger text-muted"><strong>{{ $errors->first('email') }}</strong></small></p>

                                        @endif
                                    </fieldset>
                                    <fieldset class="form-group floating-label-form-group mb-1 {{ $errors->has('password') ? ' has-danger' : '' }}">
                                        <label for="password">Senha</label>
                                        <input type="password" required class="form-control {{ $errors->has('password') ? ' form-control-danger' : '' }}" name="password" id="password" placeholder="Senha">
                                        @if ($errors->has('password'))
                                            <p class="text-xs-right"><small class="danger text-muted"><strong>{{ $errors->first('password') }}</strong></small></p>
                                        @endif
                                    </fieldset>
                                    <fieldset class="form-group row">
                                        <div class="col-md-12 col-xs-12 float-sm-left text-xs-center text-sm-right">
                                            <a href="/" class="card-link">Esqueceu sua senha?</a>
                                        </div>
                                    </fieldset>
                                    <button type="submit" class="btn btn-outline-primary btn-block">
                                        <i class="ft-unlock"></i> Login
                                    </button>
                                    <a href="#" id="fb-login" class="btn btn-social mb-1 mr-1 btn-block btn-outline-facebook">
                                        <span class="fa fa-facebook"></span>
                                        <span>facebook</span>
                                    </a>
                                </form>
                            </div>
                            <p class="card-subtitle line-on-side text-muted text-xs-center font-small-3 mx-2 my-1"><span>Não tem cadastro?</span></p>
                            <div class="card-block">
                                <a href="/register" class="btn btn-outline-danger btn-block">
                                    <i class="ft-user"></i> Cadastre-se
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


<script src="{{ asset('user/vendors/js/vendors.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/extensions/jquery.knob.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/scripts/extensions/knob.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/raphael-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/morris.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="{{ asset('user/vendors/js/charts/jvector/jquery-jvectormap-2.0.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/jvector/jquery-jvectormap-world-mill.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.perfect-scrollbar/0.7.0/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="{{ asset('user/data/jvector/visitor-data.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/chart.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/charts/jquery.sparkline.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/vendors/js/extensions/unslider-min.js') }}" type="text/javascript"></script>
<link href="{{ asset('user/css/core/colors/palette-climacon.css') }}" rel="stylesheet" type="text/css" >
<link href="{{ asset('user/fonts/simple-line-icons/style.min.css') }}" rel="stylesheet" type="text/css" >
<script src="{{ asset('user/js/core/app-menu.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/core/app.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/scripts/customizer.min.js') }}" type="text/javascript"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/2.3.0/mustache.min.js" type="text/javascript"></script>
<script src="https://cdn.firebase.com/js/client/2.0.4/firebase.js"></script>
<script src="{{ asset('user/js/jquery.mask.min.js') }}"></script>
<script src="{{ asset('user/js/facebook.js') }}" type="text/javascript"></script>
<script src="{{ asset('user/js/octuz.js') }}" type="text/javascript"></script>
<script>
    (function(h,e,a,t,m,p) {
        m=e.createElement(a);m.async=!0;m.src=t;
        p=e.getElementsByTagName(a)[0];p.parentNode.insertBefore(m,p);
    })(window,document,'script','https://u.heatmap.it/log.js');
</script>

<script type="text/javascript">
    window.smartlook||(function(d) {
        var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
        var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
        c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
    })(document);
    smartlook('init', 'b252a61dec3e0b28906cf8ab56309467a9fd615b');
</script>

<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1698839770440223');
    fbq('track', 'PageView');
    fbq('track', 'Lead');
    fbq('track', 'CompleteRegistration');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1698839770440223&ev=PageView&noscript=1"
    /></noscript>
<style>
    .btn-social-icon > :first-child, .btn-social > :first-child {
        position: inherit;
        font-size: 1.2em;
        border-right: none;
    }

    .btn.btn-social {
        padding: .1rem;
        text-align: center;
    }

</style>
</body>
</html>
$.get("https://octuz.com/api/empresa?find=", function(data){
    $(".typehead").typeahead({
        display: 'nome',
        source:data
    });
},'json');

$('.btn-resolve').on('click', function(){
    var empresa = $(".typehead").val();

    if(empresa === ''){
        empresa = 'null';
    }

    $.get('/api/empresa/'+empresa, function (data){
        if(data.success === true){
            if(data.data.category_id === 2){
                $("#employer_id").val(data.data.id)
                $('#modalTelefonica').modal('toggle')
            }
        } else {
            toastr.error(data.message)
        }
    })
})

$(".btn-login-og").on('click', function(){
    $.post('/api/login', {email: $("#email_login").val(), password: $("#password_login").val()}, function(data){
        if(data.success === true){
            $("#csrf").val(data.data.encrypted_hash);
            $("form").submit();
        } else {
            $(".error-login").empty()
            $(".error-login").append('<div class="alert alert-danger fade in alert-dismissable">\
                                        <strong>Atenção!</strong> '+data.message+'\
                                    </div>')

        }

    })
});

$(".btn-register-og").on('click', function(){

   $.post('/api/register',
       {
           name: $("#name").val(),
           password:$("#password").val() ,
           password_confirmation: $("#password_confirmation").val(),
           address: $("#address").val(),
           fone: $("#fone").val(),
           contact_fone: $("#contact_fone").val(),
           dt_nascimento: $("#dt_nascimento").val() ,
           cpf: $("#cpf").val(),
           employer_id: $("#employer_id").val(),
           email: $("#email_register").val()
       })
       .done(function(data) {
           if(data.success === true){
               $("#csrf").val(data.data.encrypted_hash);
               $("#account_id").val(data.data.account);

               $("form").submit();
           } else {
               $("#error-register").append('<div class="alert alert-danger fade in alert-dismissable">\
                                        <strong>Atenção!</strong> '+data.message+'\
                                    </div>')
           }

       })
       .fail(function(data) {
           $.each($("input"), function(v, el){
               $(el).parent().find('small').remove()
               $(el).parent().removeClass("has-error")
           })

           $.each(data.responseJSON, function(key, val){
                $("#"+key).parent().append('<small style="" class="help-block">'+val+'</small>')
                $("#"+key).parent().addClass("has-error")
           })
       })
});

$(".btn-update-data").on('click', function(){

    $.post('/api/update/profile',
        {
            csrf: $("#csrf").val(),
            name_upd: $("#name_upd").val(),
            address_upd: $("#address_upd").val(),
            fone_upd: $("#fone_upd").val(),
            contact_fone_upd: $("#contact_fone_upd").val(),
            dt_nascimento_upd: $("#dt_nascimento_upd").val() ,
            cpf_upd: $("#cpf_upd").val(),
        })
        .done(function(data) {
            if(data.success === true){
                $("form").submit();
            } else {
                $("#error-updater").append('<div class="alert alert-danger fade in alert-dismissable">\
                                        <strong>Atenção!</strong> '+data.message+'\
                                    </div>')
            }

        })
        .fail(function(data) {
            $.each($("input"), function(v, el){
                $(el).parent().find('small').remove()
                $(el).parent().removeClass("has-error")
            })

            $.each(data.responseJSON, function(key, val){
                $("#"+key).parent().append('<small style="" class="help-block">'+val+'</small>')
                $("#"+key).parent().addClass("has-error")
            })
        })
});


$('#radioBtn a').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);

    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})



$('#modalTelefonica').modalSteps();


$('.upload-btn').on('click', function (){
    $('#upload-input').click();
    $('.progress-bar').text('0%');
    $('.progress-bar').width('0%');
});

$('#upload-input').on('change', function(){

    var files = $(this).get(0).files;

    if (files.length > 0){
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            formData.append('uploads[]', file, file.name);
        }

        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(data){
                $.each(data.data, function (i, v){
                    $("#recipes").append('<input type="hidden" id="files" name="files[]" value="'+v+'">')
                })
            },
            xhr: function() {
                var xhr = new XMLHttpRequest();

                xhr.upload.addEventListener('progress', function(evt) {

                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        $('.progress-bar').text(percentComplete + '%');
                        $('.progress-bar').width(percentComplete + '%');
                        if (percentComplete === 100) {
                            $('.progress-bar').html('100%');
                        }
                    }

                }, false);

                return xhr;
            }
        });
    }
});

$("#fone").mask('(00) 0 0000-0000');
$("#contact_fone").mask('(00) 0 0000-0000');
$("#dt_nascimento").mask('00/00/0000');
$("#cpf").mask('000.000.000-00');
$("#fone_upd").mask('(00) 0 0000-0000');
$("#contact_fone_upd").mask('(00) 0 0000-0000');
$("#dt_nascimento_upd").mask('00/00/0000');
$("#cpf_upd").mask('000.000.000-00');


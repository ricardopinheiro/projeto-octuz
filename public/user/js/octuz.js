
getListaIssue();

function getListaIssue()
{
    block();

    $(".issues").empty();
    $.get('/templates/lista.mst', function(tpl) {
        var user = $("body").data('id');
        $.get("/api/user/"+user+"/issue")
            .done(function( data ) {
                $.each(data.data, function(key,val) {
                    $(".issues").append(Mustache.to_html(tpl, val));
                });
            });

    });
}

function getProfileIssue()
{
    $("#profile-issue").empty();
    $.get('/templates/user.mst', function(tpl) {
        var issue = getHashValue("hash");
        $.get("api/issue/"+issue)
            .done(function( data ) {
                console.log(data)
            });
    });
}

function block() {
    return $.blockUI({
        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
        overlayCSS: {
            backgroundColor: '#FFF',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: '#fff'
        }
    });
}

function getChatIssue () {
    setTimeout(function(){ block() }, 1);
    $("#social-cards").empty()
    $.get('/templates/body-content.mst', function(tpl) {
        $("#social-cards").append(Mustache.to_html(tpl, {}))
    });

    getProfile();
    getChat()

};

function getProfile()
{
    $("#profile-issue").empty();
    $.get('/templates/profile.mst', function(tpl) {
        var issue = getHashValue();
        $.get("api/issue/"+issue)
            .done(function( data ) {
                $("#profile-issue").append(Mustache.to_html(tpl, data.data));
            });
    });
}

function getChat()
{
    $(".chat-content").empty();
    $.get('/templates/chat.mst', function(tpl) {
        var issue = getHashValue();
        firebaseListen();
        var user = $("body").data('id');
        $.get("api/issue/"+issue+"/user/"+user+"/messages")
            .done(function( data ) {
                $.each(data.data, function(key,val) {
                    $(".chat-content").append(Mustache.to_html(tpl, val));
                });

                $(".ps").scrollTop( $(".chat-content").prop( "scrollHeight" ));
                $(".ps").perfectScrollbar('update');
            });
    }).done(function() {
        setTimeout(function(){
            $.unblockUI();
            if(!window.width > 480){
                $(".nav-menu-main").click()
            }
        }, 1);
    });

}

function firebaseListen()
{
    var issue = getHashValue();
    var messagesRef = new Firebase('https://octuz-799de.firebaseio.com/local/message_users/'+issue+'/');
    messagesRef.limitToLast(1).on('child_added', function (snapshot) {
        var data = snapshot.val();
        $.get('/templates/chat.mst', function(tpl) {
            var user = $("body").data('id');
            data.self = user === data.user.id ? true : false;
            if(data.self === false){
                $(".chat-content").append(Mustache.to_html(tpl, data));
                $(".ps").scrollTop( $(".chat-content").prop( "scrollHeight" )+100 );
                $(".ps").perfectScrollbar('update');
            }
        });
    });

}

function getHashValue() {
    return window.location.hash.substr(1);
}


function send() {
    var message = $("#message-chat").val();
    var issue = getHashValue();
    var user = $("body").data('id');
    d = new Date();
    $.get('/templates/chat.mst', function(tpl) {
        var data = {
            message: message,
            user: {
                path_url: $(".avatar-img").attr('src'),
            },
            created_at: d.getDay() + '/'+ d.getUTCMonth() + '/' + d.getFullYear(),
            self: true
        }

        $(".chat-content").append(Mustache.to_html(tpl, data));
        $(".ps").scrollTop( $(".chat-content").prop( "scrollHeight" ) );
        $(".ps").perfectScrollbar('update');
    })

    $.post("api/issue/"+issue+"/user/"+user+"/messages", { message: message, user_id: user, issue_id: issue, status: 0 })

    $("#message-chat").val('');
}


$(window).on("load", function() {
    $.unblockUI()
});

$(".btn-unlock").on('click', function(){
    $.each($("input"), function(i, el){
        $(el).attr('readonly', false);
        $(".btn-submit-f").attr("disabled", false)
    })
})

function editAccount(el){
    var id = $(el).data("id");
    var user = $("body").data('id');

    $.get('/api/user/'+user+'/account/'+id, function(data){
        $("#form-acc").attr('action', '/painel/account/update/'+data.data.id)
        $("#form-del").attr('action', '/painel/account/update/'+data.data.id)
        $("#telefone_modal").val(data.data.numero_conta)
        $("#login_modal").val(data.data.login)
        $("#senha_modal").val(data.data.senha)
        $("#iconForm").modal('toggle')
    })
}

$('#iconForm').on('hidden.bs.modal', function () {
    $("#telefone_modal").val('')
    $("#login_modal").val('')
    $("#senha_modal").val('')
})

$('#cpf').mask('000.000.000-00');
$('#fone').mask('(00) 0 0000-0000');
$('#data').mask('00/00/0000');
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//Auth::logout();
Route::post('facebook/login', 'Auth\\SocialController@facebook');
Route::post('entrar', 'Painel\\AuthController@login');
Route::post('upload', 'UploadController@upload');

Route::group(['prefix' => 'painel', 'as' => 'painel.', 'middleware' => 'auth'], function(){
    Route::get('/', 'Painel\\HomeController@index')->name('index');
    Route::get('/perfil', 'Painel\\HomeController@perfil')->name('perfil');
    Route::post('/perfil', 'Painel\\HomeController@update')->name('update.perfil');
    Route::post('/account/update/{id}', 'Painel\\AccountController@update');
    Route::post('/cadastrar/caso', 'Painel\\HomeController@caso');
    Route::delete('/account/update/{id}', 'Painel\\AccountController@destroy');
});


Route::group(['prefix' => 'admin', 'as' => 'admin.'], function(){
    Route::group(['middleware' => 'CheckRole'], function(){
        Route::get('/', 'Atendimento\\HomeController@index')->name('index');
        Route::get('/casos', 'Atendimento\\PipeController@index');
    });
    Route::get('/login', 'Atendimento\\AuthController@login');
    Route::post('/login', 'Atendimento\\AuthController@postLogin');
});

Auth::routes();

Route::get('/home', function(){
    return redirect('/painel');
})->name('home');
